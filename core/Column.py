from typing import Iterable


class Column:
    def __init__(self, x: int, ys: int, ye: int, front: bool):
        self.x = x
        self.ys = ys
        self.ye = ye
        self.front = front
