from typing import Iterable


class RunnerSetting:
    """Represents the stats for a specimens times to reach easy and difficult spots with
    its speed and time taken to turn.
    """

    def __init__(self, arm_tti_easy: int, arm_tti_diff: int, speed: float, ttt: int):
        """Instantiats the object representing the statistics of a specimen.

        Args:
            arm_tti_easy (int): Time taken to investigate an easy spot
            arm_tti_diff (int): Time taken to investigate a difficult spot
            speed (float): Speed of the object around the grid
            ttt (int): Time taken by the object to turn from vine to vine
        """
        self.arm_tti_easy = arm_tti_easy
        self.arm_tti_diff = arm_tti_diff
        self.speed = speed
        self.ttt = ttt
