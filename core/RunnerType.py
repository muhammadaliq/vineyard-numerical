from enum import Enum


class RunnerType(Enum):
    """Represents the enum type for each of the field runners
    
    HUMAN = 1

    ROBOT = 2

    AUTO = 3
    """
    HUMAN = 1
    ROBOT = 2
    AUTO = 3