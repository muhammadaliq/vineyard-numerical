import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.dates as md
import random
import seaborn as sns

from core.Column import Column
from core.RunnerSetting import RunnerSetting
from core.RunnerType import RunnerType

sns.set()


class SimulationSettings:
    """Contains the settings to run the simulation
    """

    def __init__(self, file_name: str, for_yield: bool = False):
        """Initializes the simulation settings based on hardcoded values and files passed as input.

        Args:
            file_name (str): The filename from which to extract the y coordinates
            for_yield (bool, Optional): If we need the x maps for the yield map. Defaults to False.

        """

        self.font_size = 15

        self.for_yield = for_yield
        self.yield_length = 5

        self.easy_arm_times = [27.68, 25.72, 23.70, 24.56, 27, 23.59, 24.23]
        self.diff_arm_times = [66.40, 42.12, 51.88, 47.71, 46.37, 44.54, 47.35]

        self.spec = {
            RunnerType.HUMAN: RunnerSetting(5, 5, 1.25, 5),
            RunnerType.ROBOT: RunnerSetting(24, 50, 1.25, 10),
            RunnerType.AUTO: RunnerSetting(24, 50, 1.25, 10)
        }

        self.column_width = 1  # Width of each column
        self.space_vine = 3  # Space between each column (meters)

        self._make_grid_specs(file_name)

    def make_x_map(self):
        """Creates a dictionary of each x coordinate as key and its Column as value

        Returns:
            dict(Column): Returns dictionary of each x coordinate as key and its Column as value
        """
        return {col.x: col for col in self.merged}

    def plot_arm_times_bar(self, bar_width: int = 0.1):
        """Creates the bar plot of the arm times based on settings.

        Args:
            bar_width (int, optional): The width of the arm times needed. Defaults to 0.1.
        """
        easy_times = [self.spec[RunnerType.HUMAN].arm_tti_easy,
                      self.spec[RunnerType.ROBOT].arm_tti_easy, self.spec[RunnerType.AUTO].arm_tti_easy]
        diff_times = [self.spec[RunnerType.HUMAN].arm_tti_diff,
                      self.spec[RunnerType.ROBOT].arm_tti_diff, self.spec[RunnerType.AUTO].arm_tti_diff]

        font_size = self.font_size
        _, ax = plt.subplots(figsize=(12, 8))

        # Set position of bar on X axis
        br1 = np.arange(len(easy_times))
        br2 = [x + bar_width for x in br1]

        ax.bar(br1, easy_times, color='blue',
               width=bar_width, label='Easy Spot')
        ax.bar(br2, diff_times, color='green',
               width=bar_width, label='Difficult Spot')
        ax.set_title("Times to Reach Different Spots", fontsize=font_size)
        ax.set_ylabel("Seconds", fontsize=font_size)
        ax.set_xlabel("Test Subjects", fontsize=font_size)
        ax.set_xticks([r + bar_width/2 for r in range(len(easy_times))],
                      ['Human', 'VR Robot', 'Non-Immersive Robot'], fontsize=font_size)
        ax.legend(bbox_to_anchor=(1.0, 1),
                  loc='upper left', fontsize=font_size)

    def plot_arm_times_line(self):
        """Creates a line plot of the arm times for easy and difficult spot variances
        """
        font_size = self.font_size
        _, ax = plt.subplots(figsize=(12, 8))
        x = list(range(1, len(self.easy_arm_times) + 1, 1))

        ax.plot(x, self.easy_arm_times, color='b', label="Easy times")
        ax.plot(x, self.diff_arm_times, color='r', label="Difficult times")
        ax.set_title("Times to Reach Different Spots", fontsize=font_size)
        ax.set_ylabel("Seconds", fontsize=font_size)
        ax.set_xlabel("Experiments", fontsize=font_size)
        ax.legend(bbox_to_anchor=(1.0, 1),
                  loc='upper left', fontsize=font_size)

    def get_grid_preimeter(self):
        """Get the total length of each column

        Returns:
            int: total length of grid
        """
        return sum(map(lambda x: x.ye - x.ys, self.col_x))

    def make_spots(self, spots: int = 30):
        """Creates a random iteration of spots based on the settings. Each iteration will have a random arrangement of spots,
        but the number of spots will be consistently based on the settings. A fix number of those spots will be difficult to reach, 
        and number of difficult spots will be returned. 

        Args:
            spots (int, Optional): The minimum number of spots to make. Defaults to 30

        Returns:
            list(tuple), list(tuple), int: returns the list of x and y location of a spot,
            and number of difficult spots.
        """

        if self.for_yield:
            return self.yield_spots, 0

        diff = random.randint(0, spots // 1.2)  # Difficult spots
        rng = range(len(self.merged))
        locs = []

        for _ in range(spots):
            ch = random.choice(rng)
            locs.append((self.merged[ch].x, random.randint(
                self.merged[ch].ys, self.merged[ch].ye)))

        return locs, diff

    # New representation of fully customized grid

    def __get_y_coords(self, file_name: str):
        """Returns the y coordinates from the file passed as parameter

        Args:
            file_name (str): The filename from which to extract the y coordinates

        Returns:
            List(tuple): List of (ys, ye) -> ys and ye are starting and ending y coordinates
        """
        s = ""

        with open(file_name) as o:
            s = o.readlines()

        # Convert "ys,ye\n" strings to tuple (ys, ye) -> Where ys is starting y coordinate, ye is ending y coordinate
        ret = list(
            map(lambda x: tuple(map(lambda y: int(y), x.replace("\n", "").split(","))), s))

        if self.for_yield:
            app = self.yield_length // 2
            tr = [list(i) for i in zip(*ret)]

            for i in range(len(tr)):
                tr.insert((2 * i) + 1, list(tr[2 * i]))

            spots = []
            cols = []
            ins = True

            for x, ls in enumerate(tr):
                x = (x * self.space_vine) + self.column_width
                end = -1
                start = 0

                for i, elem in enumerate(ls):
                    if end == -1 and elem > 0:
                        end = len(ls) - i

                    if end > -1 and elem == 0:
                        start = len(ls) - i

                    if elem == 1 and ins:
                        j = len(ls) - i
                        spots.append((x, (j * self.yield_length) + app))

                cols.append((start * self.yield_length,
                            end * self.yield_length))
                 
                ins = not ins

            return cols, spots

        return ret

    def _make_grid_specs(self, file_name):
        """Instantiate the grid settings pertaining to the columns and grid heights

        Args:
            file_name (str): Name of the file to extract columns from.
        """
        self.col_y = self.__get_y_coords(file_name)

        if self.for_yield:
            self.col_y, self.yield_spots = self.col_y

        self.grid_cols = len(self.col_y)  # Vine columns

        self.col_x = list(
            range(0, (self.grid_cols * self.space_vine), self.space_vine))
        self.col_x_end = list(range(
            self.column_width, (self.grid_cols * self.space_vine) + self.column_width, self.space_vine))

        self.col_x = [Column(x, y[0], y[1], True)
                      for x, y in zip(self.col_x, self.col_y)]
        self.col_x_end = [Column(x, y[0], y[1], False)
                          for x, y in zip(self.col_x_end, self.col_y)]

        # lits of columns class
        self.merged = self.col_x + self.col_x_end

        self.max_height = max(map(lambda x: x.ye, self.col_x))
