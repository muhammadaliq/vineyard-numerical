import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.dates as md
import random
import time

import seaborn as sns

from core.RunnerSetting import RunnerSetting
from core.RunnerType import RunnerType
from core.SimulationSettings import SimulationSettings
from core.Column import Column

sns.set()


class Simulator:
    """ Main simulation class that is responsible for conducting various simulations depending on the settings provided
    """

    def __init__(self, settings: SimulationSettings):
        """Instatiates the simulation object with the settings provided.

        Args:
            settings (SimulationSettings): Settings on which the simulaitons will be based on
        """
        self._settings = settings
        self._started = False
        self._experiment_runs = 100  # Number of times to perform the monte carlo experiments

    def __enter__(self):
        """Used to create the simulation environment

        Raises:
            ValueError: If the environment is not set up
        """
        if not self._started:
            self._started = True
            self._spot_counts = list(range(20, 41, 10))
            self._s_arr, self._d_arr = [], []
            self._total_hum_f, self._total_rob_f, self._total_auto_f = [], [], []
            self._total_hum_s, self._total_rob_s = [], []

            for cnt in self._spot_counts:
                st, dt = [], []
                for _ in range(self._experiment_runs):
                    s, d = self._settings.make_spots(cnt)
                    st.append(s)
                    dt.append(d)

                self._s_arr.append(st)
                self._d_arr.append(dt)
        else:
            raise ValueError("Simulation already Started!")

    def __exit__(self, exc_type, exc_value, tb):
        """Used to destroy the simulation environment

        Raises:
            ValueError: If the environment is not set up
        """
        if self._started:
            self._started = False
            self._spot_counts = []
            self._s_arr, self._d_arr = [], []
            self._total_hum_f, self._total_rob_f, self._total_auto_f = [], [], []
            self._total_hum_s, self._total_rob_s = [], []
        else:
            raise ValueError("No Simulation Started!")

    def print_first_round_results(self):
        """Create the results of the monte-carlo simulation for multiple spots.
        Prints the results with percentage difference with human performance.

        Raises:
            ValueError: If the environment is not set up
        """
        if not self._started:
            raise PermissionError("Need to start simulation environment!")

        for sp in range(len(self._spot_counts)):
            av_hum = []
            for r in range(self._experiment_runs):
                s, d = self._s_arr[sp][r], self._d_arr[sp][r]
                av_hum.append(self._calculate_grid_time(
                    RunnerType.HUMAN, s, d, True))

            self._total_hum_f.append(list(av_hum))
            print("Human Simulation with", str(self._spot_counts[sp]) + " Spots:", time.strftime(
                '%H hrs %M mins', time.gmtime(np.average(av_hum))), "")
            av_hum = []

        i = 0
        for sp in range(len(self._spot_counts)):
            av_rob = []
            for r in range(self._experiment_runs):
                s, d = self._s_arr[sp][r], self._d_arr[sp][r]
                av_rob.append(self._calculate_grid_time(
                    RunnerType.ROBOT, s, d, False))

            self._total_rob_f.append(list(av_rob))
            hum_avg, avg = np.average(
                self._total_hum_f[i]), np.average(av_rob)
            perc_diff = ((avg - hum_avg)/hum_avg) * 100

            print("VR Robot Simulation with", str(self._spot_counts[sp]) + " Spots:",
                  time.strftime('%H hrs %M mins', time.gmtime(np.average(av_rob))), "Percentage difference with human: %.1f" % perc_diff)

            av_rob = []

            i += 1

        i = 0
        for sp in range(len(self._spot_counts)):
            av_auto = []
            for r in range(self._experiment_runs):
                s, d = self._s_arr[sp][r], self._d_arr[sp][r]
                av_auto.append(self._calculate_grid_time(
                    RunnerType.AUTO, s, d, True))

            self._total_auto_f.append(list(av_auto))
            hum_avg, avg = np.average(
                self._total_hum_f[i]), np.average(av_auto)
            perc_diff = ((avg - hum_avg)/hum_avg) * 100

            print("Autonomous Robot Simulation with", str(self._spot_counts[sp]) + " Spots:",
                  time.strftime('%H hrs %M mins', time.gmtime(np.average(av_auto))), "Percentage difference with human: %.1f" % perc_diff)

            av_auto = []

            i += 1

    def print_second_round_results(self):
        """Create the results of the monte-carlo simulation for multiple spots.
        Prints the results with percentage difference with human performance.

        Raises:
            ValueError: If the environment is not set up
        """
        if not self._started:
            raise PermissionError("Need to start simulation environment!")

        for sp in range(len(self._spot_counts)):
            av_hum = []
            for r in range(self._experiment_runs):
                s, d = self._s_arr[sp][r], self._d_arr[sp][r]
                av_hum.append(self._calculate_grid_time(
                    RunnerType.HUMAN, s, d, True, False))

            self._total_hum_s.append(list(av_hum))
            print("Human Simulation with", str(self._spot_counts[sp]) + " Spots:", time.strftime(
                '%H hrs %M mins', time.gmtime(np.average(av_hum))), "")
            av_hum = []

        i = 0
        for sp in range(len(self._spot_counts)):
            av_rob = []
            for r in range(self._experiment_runs):
                s, d = self._s_arr[sp][r], self._d_arr[sp][r]
                av_rob.append(self._calculate_grid_time(
                    RunnerType.ROBOT, s, d, False, True))

            self._total_rob_s.append(list(av_rob))
            hum_avg, avg = np.average(
                self._total_hum_s[i]), np.average(av_rob)
            perc_diff = ((avg - hum_avg)/hum_avg) * 100

            print("VR Robot Simulation with", str(self._spot_counts[sp]) + " Spots:",
                  time.strftime('%H hrs %M mins', time.gmtime(np.average(av_rob))), "Percentage difference with human: %.1f" % perc_diff)

            av_rob = []

            i += 1

    def plot_first_round_result(self, y_low: int, y_high: int):
        """Plot graph representing the experiment results of 30 spots on each runner.
        This will plot the results for the first round.

        Args:
            y_low (int): The Lower limit for y axis
            y_high (int): The upper limit for y axis

        Raises:
            PermissionError: If the environment is not set up
        """
        if not self._started:
            raise PermissionError("Need to start simulation environment!")

        self._plot_round_result(
            self._total_hum_f[1], self._total_rob_f[1], self._total_auto_f[1], y_low, y_high)

    def plot_second_round_result(self, y_low: int, y_high: int):
        """Plot graph representing the experiment results of 30 spots on each runner.
        This will plot the results for the second round.

        Args:
            y_low (int): The Lower limit for y axis
            y_high (int): The upper limit for y axis

        Raises:
            PermissionError: If the environment is not set up
        """
        if not self._started:
            raise PermissionError("Need to start simulation environment!")

        self._plot_round_result(
            self._total_hum_s[1], self._total_rob_s[1], None, y_low, y_high)

    def show_sample_grid(self):
        """Plot the grid based on random spots.
        """
        self._plot_sample_grid(
            self._settings.make_spots(30)[0])

    def show_first_round_trace(self):
        """Show the trace of the runners for the first round on the grid

        Raises:
            PermissionError: If the environment is not set up
        """
        if not self._started:
            raise PermissionError("Need to start simulation environment!")

        self._plot_sample_grid(self._s_arr[1][0], False,
                               (self._get_trace_coordinates(RunnerType.HUMAN, True), "Path by Human"))

        self._plot_sample_grid(self._s_arr[1][0], False,
                               (self._get_trace_coordinates(RunnerType.ROBOT, False), "Path by Immersive Robot"))

        self._plot_sample_grid(self._s_arr[1][0], False,
                               (self._get_trace_coordinates(RunnerType.AUTO, True), "Path by Autonomous Robot"))

    def show_second_round_trace(self):
        """Show the trace of the runners for the second round on the grid

        Raises:
            PermissionError: If the environment is not set up
        """
        if not self._started:
            raise PermissionError("Need to start simulation environment!")

        self._plot_sample_grid(self._s_arr[1][0], False,
                               (self._get_trace_coordinates(RunnerType.HUMAN, True), "Path by Human"))

        self._plot_sample_grid(self._s_arr[1][0], False,
                               (self._get_trace_coordinates(RunnerType.ROBOT, False, self._s_arr[1][0]), "Path by Immersive Robot"))

    def print_yield_result(self):
        """Create the results of the yield based simulation for given spots.
        Prints the results with percentage difference with human performance.

        Raises:
            PermissionError: Raise error if the simulation has entered or grid is not a yield grid
        """

        if self._started or not self._settings.for_yield:
            raise PermissionError("Incorrect yield configuration!")

        sp = self._settings.make_spots()[0]
        ht = self._calculate_grid_time(RunnerType.HUMAN, sp, 0, True, True)
        rt = self._calculate_grid_time(RunnerType.ROBOT, sp, 0, False, True)

        print("Human Simulation with", str(len(sp)) + " Low Yield Spots:", time.strftime(
            '%H hrs %M mins', time.gmtime(ht)), "")

        print("VR Robot Simulation with", str(len(sp)) + " Low Yield Spots:",
              time.strftime('%H hrs %M mins', time.gmtime(rt)), "Percentage difference with human: %.1f" % (((rt - ht)/ht) * 100))

    def show_yield_trace(self):
        """Show the trace of the runners for the yield on the grid

        Raises:
            PermissionError: Raise error if the simulation has entered or grid is not a yield grid
        """
        if self._started or not self._settings.for_yield:
            raise PermissionError("Incorrect yield configuration!")

        sp = self._settings.make_spots()[0]

        self._plot_sample_grid(sp, True,
                               (self._get_trace_coordinates(RunnerType.HUMAN, True), "Path by Human"))

        self._plot_sample_grid(sp, True,
                               (self._get_trace_coordinates(RunnerType.ROBOT, False, sp), "Path by Immersive Robot"))

    def _get_trace_coordinates(self, runner: RunnerType, is_auto: False, infections: list = None):
        """Create the trace for each runner including both rounds.

        Args:
            runner (RunnerType): The runner for which the trace needs to be made for a given grid
            is_auto (False): If the runner is autonomous
            infections (list, optional): The infectious spots that needed to be visited by the runner.
            This allows the trace to be made for minimized paths. Defaults to None.

        Returns:
            list: List of x, y coordinates to make the trace
        """
        coords = []

        dist = self._settings.space_vine / 2 if is_auto else self._settings.space_vine / 4
        top = self._settings.space_vine / 4
        from_top = True

        if infections and runner == RunnerType.ROBOT:
            self._second_visit_travel(infections, coords)
        elif is_auto:
            lz = list(zip(self._settings.col_x, self._settings.col_x_end))

            for i, (xs, xe) in enumerate(lz):
                if from_top:
                    mh = xs.ye if i == 0 else max(lz[i - 1][0].ye, xs.ye)

                    coords.extend([(xs.x - dist, xs.ys - top), (xs.x - dist, mh + top),
                                   (xe.x, mh + top)])
                else:
                    coords.extend([(xs.x - top, xs.ye + top), (xs.x - top, xs.ys - top),
                                   (xe.x, xs.ys - top)])

                from_top = not from_top

            last = lz[-1][1]

            if from_top:
                coords.extend([(last.x + dist, last.ys - top),
                              (last.x + dist, last.ye + top)])
            else:
                coords.extend([(last.x + top, last.ye + top),
                              (last.x + top, last.ys - top)])

        else:
            for xs, xe in zip(self._settings.col_x, self._settings.col_x_end):
                coords.extend([(xs.x - dist, xs.ys - top), (xs.x - dist, xs.ye + top),
                               (xe.x + dist, xe.ye + top), (xe.x + dist, xe.ys - top)])

        return coords

    def _plot_sample_grid(self, spots: list, low_yield: bool = False, trace: tuple = None):
        """Create a sample grid representing the field for simulations.

        Args:
            spots (list): The diseased spots to show on the grid
            low_yield (bool, optional): Change the spots represented on grid 
            to either represent diseased spots or low yield spots. Defaults to False.
            trace (tuple, optional): The details of the trace including the trace coordinates in order and its heading. 
            Defaults to None
        """
        fontsize = self._settings.font_size + 4
        _, ax = plt.subplots(figsize=(40, 45))

        for i in range(len(self._settings.col_x)):
            r = patches.Rectangle((self._settings.col_x[i].x, self._settings.col_x[i].ys),
                                  self._settings.column_width, self._settings.col_x[i].ye, edgecolor="b", facecolor="b")

            ax.add_patch(r)

        x, y = list(zip(*spots))

        if not low_yield:
            ax.scatter(x, y, s=fontsize*20, c='r', marker='x', label='Disease Spots')
        else:
            x = list(map(lambda z: z - 0.5, x))
            ax.scatter(x, y, s=fontsize*75, c='g', marker='o', label='Low yield Spots')

        if trace:
            coord, head = trace
            x, y = list(zip(*coord))

            ax.plot(x, y, color='black', label=head)
            for i in range(len(coord) - 1):
                ax.annotate("", xytext=(x[i], y[i]), xy=(
                    x[i + 1], y[i + 1]), arrowprops=dict(arrowstyle="->", connectionstyle="arc3",
                                                         linewidth=2.5,
                                                         shrinkA=2, shrinkB=2,
                                                         mutation_scale=15, color='black'))

        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        ax.set_title("Vineyard", fontsize=fontsize)
        ax.legend(bbox_to_anchor=(1.0, 1), loc='upper left', fontsize=fontsize)

    def _plot_round_result(self, human: list, robo: list, auto: list, y_low: int, y_high: int):
        """Plot the result of the monte carlo simulations. Wtih Y axis being the time taken
        and X axis representing the experiment number.

        Args:
            human (list): Human experiment times to plot
            robo (list): Robot experiment times to plot
            auto (list): autonomous robot times to plot
            y_low (int): The Lower limit for y axis
            y_high (int): The upper limit for y axis
        """
        def time_shift(time_val):
            t = time.gmtime(time_val)
            return t.tm_hour + ((t.tm_min) / 60)

        _, ax = plt.subplots(figsize=(25, 12))

        x = list(range(self._experiment_runs))
        human = list(map(lambda x: time_shift(x), human))
        robo = list(map(lambda x: time_shift(x), robo))

        if auto:
            auto = list(map(lambda x: time_shift(x), auto))

        ax.plot(x, human, color='r', label='Human Experiments')
        ax.plot(x, robo, color='g', label='VR Experiments')

        if auto:
            ax.plot(x, auto, color='b', label='Non-Immersive Robot Experiments')

        print(time.strftime("%H", time.gmtime(human[0])))
        ax.set_title("Experiments for 30 spots with Random Difficulty",
                     fontsize=self._settings.font_size)
        ax.set_ylabel("Time (hrs)", fontsize=self._settings.font_size)
        ax.set_xlabel("Experiment #", fontsize=self._settings.font_size)
        ax.set_ylim([y_low, y_high])

        ax.legend(bbox_to_anchor=(1.0, 1), loc='upper left',
                  fontsize=self._settings.font_size)

    def _get_distance_array(self,
                            start: tuple,
                            infections: list,
                            x_map: dict,
                            no_sort: bool = False):
        """From a given starting position, diseased spots (infections) and a map representing 
        the Column object with key as the x axis coordinate, the function calculates the shortest
        distance from start to each infection spot and assigns a weight for each spot accordingly. 
        It returns a list of tuple that represents the weight to reach a spot from a given start point,
        the distance from start to that point, the coordinate of that point, and if the point is in the
        same column as the starting coordinate. The list is then sorted in descending order before returning 
        depending on the no_sort variable.

        Shortest distance from the start to a given diseased spot is calculated such that we find all possible
        distance to the point, then take the minimum to reach it. 

        Maximum height of columns (mh) = max of height from start to starts column and  height from start to targets column (upper end)

        Maximum Dip of columns (md) = max of dip from start to starts column and  dip from start to targets column (lower end)

        The above calculations only represent the vertical distance traveled to get out or meet the height of a column.

        Distance from top =  distance between start y and mh + distance between target y and mh

        Distance from bot =  distance between start y and md + distance between target y and md

        minimum distance = minimum of distance from top or bottom

        The weighting formula is calculated such that (distance along x axis * 20) + distance along y axis. 
        The wright is calculated this way since distances of y axis are larger that x axis in general so 
        to compensate, x distances are multipled by 20. Furthermore, this allows us to give a high weight to 
        coordinates that are further along the axis, meaning coordinates in a different column will be taken in 
        order of their order from a given start position.

        Args:
            start (tuple): The x and y coordinate of our starting position
            infections (list): The diseased spots from the starting position
            x_map (dict): Dictionary mapping the x axis with its respective column specs
            no_sort (bool, optional): If True, the list of tuples will be sorted in descending order
            before returning. Defaults to False.

        Returns:
            list(tuple(int, int, tuple, bool, list)): Returns a list of tuples with the values represented in the tuple as:

            1- Weight of the coordinate

            2- Distance to the coordinate

            3- The Cordinate itself

            4- If the coordinate is in the same column as the start

            5- The trace matrix representing the trace to reach each infection from given start
        """

        dist, dist_weight, same_col, trace_mat = [], [], [], []

        for infec in infections:
            x_dist = abs(infec[0] - start[0])

            start_dist = self._settings.space_vine / 4
            start_dist = (-start_dist) if x_map[start[0]].front else start_dist
            infec_dist = self._settings.space_vine / 4
            infec_dist = (-infec_dist) if x_map[infec[0]].front else infec_dist

            top = self._settings.space_vine / 2
            tr = []

            if x_dist == 0:
                y_dist = abs(start[1] - infec[1])
                same_col.append(True)
            else:

                top_diff = max(x_map[start[0]].ye, x_map[infec[0]].ye)
                bot_diff = min(x_map[start[0]].ys, x_map[infec[0]].ys)

                path_from_top = abs(
                    top_diff - start[1]) + abs(top_diff - infec[1])
                path_from_bot = abs(
                    bot_diff - start[1]) + abs(bot_diff - infec[1])

                y_dist = min(path_from_top, path_from_bot)

                if path_from_top < path_from_bot:
                    y_dist = path_from_top
                    tr.extend([(start[0] + start_dist, top_diff + top),
                              (infec[0] + infec_dist, x_map[infec[0]].ye + top)])
                else:
                    y_dist = path_from_bot
                    tr.extend([(start[0] + start_dist, bot_diff - top),
                              (infec[0] + infec_dist, x_map[infec[0]].ys - top)])

                same_col.append(False)

            dist.append(x_dist + y_dist)
            tr.append((infec[0] + infec_dist, infec[1]))
            trace_mat.append(tr)
            # weighting x distances more to make us go towards our nearest column
            dist_weight.append(
                (x_dist * self._settings.max_height * 10) + y_dist)

        dist = list(zip(dist_weight, dist, infections, same_col, trace_mat))

        if no_sort:
            dist.sort(key=lambda x: x[0])
        return dist

    def _second_visit_travel(self, infections: list, trace: list = None):
        """Calculates the optimal distance and turns taken to visit all diseased spots.
        We start at origin, and calculate the distances from start to infected spots, and
        keep taking the spots with lowest weightage. At the end, we get the total distance
        travelled and the turns taken to acheive the task.

        Args:
            infections (list): The diseased spots to visit
            trace (list, optional): return the path taken to acheive the 
            shortest distance to get to each point. Defaults to None.

        Returns:
            int, int: The distance travelled in meters, and the turns taken
        """
        total_dist, total_turns = 0, 0
        start = (0, 0)
        x_map = self._settings.make_x_map()

        f_inf = list(infections)
        f_inf.append(
            (self._settings.merged[-1].x, self._settings.merged[-1].ys))
        f_inf = self._get_distance_array(start, f_inf, x_map, True)
        point_path = [start]

        if trace is not None:
            trace.append((-0.5, 0))

        while len(f_inf) > 0:
            _, d, start, same_col, tr = f_inf.pop(0)

            if trace is not None:
                trace.extend(tr)
                # trace.append(start)

            point_path.append(start)
            total_dist += d

            if not same_col:
                total_turns += 1

            if (len(f_inf) > 0):
                f_inf = list(list(zip(*f_inf))[2])
                f_inf = self._get_distance_array(start, f_inf, x_map)

        return total_dist, total_turns

    def _calculate_grid_time(self,
                             runner_type: RunnerType,
                             infections: list,
                             diff_spots: int,
                             is_autonomous: bool,
                             is_second_visit: bool = False):
        """Calculates the total time taken to traverse the grid given the 
        runner type, infection spots, the number of difficult spots and some contraints.

        There are total of 2 constraints on the grid time taken.

        1- If the runner is autonomous, meaning, the runner
        does not need to traverse each side of the grid to inspect, and can inspect
        both sides of the grid in parallel.

        2- If it is the second visit, the total distance travelled is minimized if the 
        runner is a robot.

        Args:
            runner_type (RunnerType): The runner that goes around the grid - for which we need to calculate the time
            infections (list): The diseased spots needed to be visited
            diff_spots (int): The amount of difficult spots in the infectious points
            is_autonomous (bool): If the runner is autonomous
            is_second_visit (bool, optional): If this is the second round. Defaults to False.

        Returns:
            int: Time taken in seconds to travel around the grid
        """

        spec = self._settings.spec[runner_type]

        total_dist, ttt = 0, spec.ttt
        inf_length = len(infections)
        grid_perimeter = self._settings.get_grid_preimeter()

        if is_second_visit and not (runner_type == RunnerType.HUMAN):
            total_dist, turns = self._second_visit_travel(infections)
            total_ttt = (total_dist / spec.speed) + (ttt * turns)
        else:
            if is_autonomous:
                total_dist = (
                    grid_perimeter) + (self._settings.space_vine * self._settings.grid_cols)
                total_ttt = (total_dist / spec.speed) + \
                    (ttt * self._settings.grid_cols)
            else:
                total_dist = (grid_perimeter * 2) + (self._settings.space_vine *
                                                     ((self._settings.grid_cols * 2) - 1))
                total_ttt = (total_dist / spec.speed) + \
                    (ttt * ((self._settings.grid_cols * 2) - 1))

        spot_time = ((inf_length - diff_spots) * spec.arm_tti_easy) + \
            (diff_spots * spec.arm_tti_diff)
        total_time = total_ttt + spot_time

        return total_time
